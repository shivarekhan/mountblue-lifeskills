# Good Practices For Software Development

## Question 1

## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

- **Gathering requirements:** Make sure to document and get feedback on requirements in a timely manner to ensure clarity and understanding among the team.
- **Always over-communicate:** Keep team members informed of changes, delays, and issues to ensure smooth progress.
- **Stuck? Ask questions:** Clearly explain the problem and provide relevant information to get effective help and solutions.
- **Get to know your teammates:** Building relationships and understanding your team members can improve communication and collaboration.
- **Be aware and mindful of other team members:** Respect your team members' time and workload when communicating and seeking help.
- **Doing things with 100% involvement:** Prioritize focus and attention to improve productivity and maintain high energy levels. Use tools and techniques to manage distractions and maintain a healthy work-life balance.

## Question 2

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

The major area I need to improve is the Gathering requirements. I decided to make notes or documentation before diving into the coding or implementation part.
