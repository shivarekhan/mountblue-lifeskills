# Focus Management

## What is Deep Work

### Question 1

### What is Deep Work?

- Focusing without distraction on a cognitively demanding task is called as Deep Work.
- Deep work increases quality of work.

### Question 2

### Paraphrase all the ideas in the above videos and this one in detail.

- Optimal duration for deep work is at least 1 hour. Not to switch between context for at least an hour during deep work.
- Deadlines/time-block schedules are kind of motivational signal, it helps one by avoiding procastinating things and avoid taking unneccessary breaks in middle.
- Deadlines should be reasonale to humane capacity, too much pressure can reduce the productivity.
- Breaks and distractions must be scheduled.
- Deep work must be made as a habbit.
- Get enough sleep.

### Question 3

### How can you implement the principles in your day to day life?

- Practice deep work for at least one hour at a stretch, without switching context.
- Keep distractions aside.
- Don't look at smartphones or social media unnecessarily, which wastes a lot of time and reduces concentration.
- Make deep work a habit, and practice it every day.
- Having adequate sleep.

### Question 4

### Your key takeaways from the video

- Having social media is not that important to stay connected with people and friends.
- Social media are addictive and we think that they give us some entertainment on a busy day, which is not true.
- By using social media, we allow ourselves to be a source of income for companies by sharing our data.
- Avoiding social media can increase our productivity in professional work.
- Social media cause anxiety and increase stress levels which are harmful to our health.
- We can develop our skills better with no social media in our life by allowing us more time to work on skills that we need.
- A glance at social media can disturb our deep work.

## Refrences

- [What is Deep Work? | Cal Newport and Lex Fridman](https://www.youtube.com/watch?v=b6xQpoVgN68)
- [Optimal duration for deep work is at least 1 hour](https://www.youtube.com/watch?v=LA6mvxwecZ0)
- [Are deadlines good for productivity?](https://www.youtube.com/watch?v=Jkl1vMNvvHU)
- [Success in a distracted world: DEEP WORK by Cal Newport](https://www.youtube.com/watch?v=gTaJhjQHcf8)
- [Quit social media | Dr. Cal Newport | TEDxTysons](https://www.youtube.com/watch?v=3E7hkPZ-HTk)
