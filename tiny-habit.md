# Tiny Habits

## Question 1

## Your takeaways from the video (Minimum 5 points)

- Real change is brought about by consistently making a large number of small choices and adjustments to daily habits, rather than trying to make one big change.
- Significant changes can be achieved by making small adjustments to daily routine, rather than relying on one large action.
- We tend to undervalue the impact of consistently making small improvements over an extended period of time.
- Breaking down the necessary change into small habits and gradually incorporating them into one's routine, can lead to success.

## Question 2

## Your takeaways from the video in as much detail as possible

To develop a habit, cultivate tiny habits, which leads to the habbit. Which can be done by following 3 process.

- Make the behavior smaller by breaking it down into manageable habits that require less motivation and have a greater impact.
- Identify an action prompt: There are three types of prompts - External, Internal and Action prompt. Use action prompt with regular behavior to trigger a new behavior rather than external or internal prompt.
- Grow habits with some shine: Celebrate after every small victories, which increases the confidence and motivation to make progress.

## Question 3

## How can you use B = MAP to make making new habits easier?

This formula says, if you are trying to develop new habbit,

- 1.Reduce the quantity(Break into smaller chunk)/ Just do the first step.
- 2.Identify an action prompt, and attach it to the habit
- 3.Celebrate the small victories.

## Question 4

## Why it is important to "Shine" or Celebrate after each successful completion of habit?

Recognizing and rewarding ourselves for small accomplishments can help maintain the motivation to continue with a habit over the long term, ultimately leading to personal improvement.

## Question 5

## Your takeaways from the video (Minimum 5 points)

- Make good habbits easier to follow.
- Make a proper plan and have clarity for achieving any behaviour.
- Environment affect one's behaviour, so look for environment that is condusive.
- Plan for Optimizing the beginning, not the ending.
- Follow Two-minute rule: If it takes two minutes or less, do it instantly, don't procastinate it.
- Be consistent, and don't break the chain of small habbits.

## Question 6

## Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

- Outcomes are about what you get. Processes are about what you do. Identity is about what you believe.
- With outcome-based habits, the focus is on what you want to achieve. With identity-based habits, the focus is on who you wish to become.
- The ultimate form of intrinsic motivation is when a habit becomes part of your identity.
- Decide the type of person you want to be. Prove it to yourself with small wins.
- The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become.
- Becoming the best version of yourself requires you to continuously edit your beliefs, and to upgrade and expand your identity.

## Question 7

## Write about the book's perspective on how to make a good habit easier?

A good habbit can be cultivated in four stages:

- **Cue**: Initiate the action, make the first move. Make the trigger obvious and easy to reach.

- **Craving**: Have cravings which attract you towards it and make it look effortless.

- **Response**: Make the habbit easier by creating fewer steps.

- **Reward**: By Rewarding yourself for small victories and celebrating it.


## Question 8
## Write about the book's perspective on making a bad habit more difficult?

A habbit can be made more difficult by

- Making the habbit harder.
- Making it ugly to indulge in the behavior.
- Increasing the friction of the behaviour.

## Question 9
## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I will read daily 10 page of a book.
- start with the easy books.

## Question 10 
## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

Stop watching small videos because they reduce attentation span.
- uninstall the instagram frpm mobile.


## Refrences
- [Forget big change, start with a tiny habit](https://www.youtube.com/watch?v=AdKUJxjn-R8)
- [TINY HABITS by BJ Fogg | Core Message](https://www.youtube.com/watch?v=S_8e-6ZHKLs)
- [1% Better Every Day - James Clear at ConvertKit Craft + Commerce 2017](https://www.youtube.com/watch?=mNeXuCYiE0U)
- [Sleep is your superpower | Matt Walker](https://www.youtube.com/watch?v=5MuIMqhT8DM)
- [Tiny Changes, Remarkable Results - Atomic Habits by James Clear](https://www.youtube.com/watch?v=YT7tQzmGRLA)
  
