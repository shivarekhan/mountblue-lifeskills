# Manage Energy not Time

## Question 1

## What are the activities you do that make you relax - Calm quadrant?

Some of the activities that make me relaxed are

- Walking
- Cooking
- listning folk songs

## Question 2

## When do you find getting into the Stress quadrant?

- When I approach deadline of a task, if I have not completed or near-completed the task.
- When I don't meet my own expectation.

## Question 3

## How do you understand if you are in the Excitement quadrant?

- When I feel satisfied by meeting my expectations.
- When i solve leetcode hard problem in best complexity
- When the daily streak becomes 100 on leetcode.
- When I complete a project within the deadline.

# Strategies to deal with Stress

## Question 4

## Paraphrase the Sleep is your Superpower video in detail.

- The speaker in the video "Sleep is Your Superpower" talks on the value of sleep and how it may enhance many parts of our lives. The speaker starts out by outlining how important sleep is for both physical and mental health as well as for the body's ability to heal and regenerate.

- The speaker continues by listing a few advantages of sleep, such as a stronger immune system, greater physical health, and better memory and cognition. The speaker adds that getting enough sleep can help with mood improvement, emotion regulation, and even weight loss.

- The speaker highlights that many people do not get enough sleep despite the fact that it has many advantages. This may be the result of several things, including stress, obligations from work or school, or lifestyle decisions. The speaker exhorts audience members to make sleep a top priority in their life.

- In order to be our best selves and function at our top levels, the speaker claims that sleep is a "superpower" that may aid us. The video's theme is that by making sleep a priority, we may enhance our general health and wellbeing and lead better, more fulfilling lives.

## Question 5

## What are some ideas that you can implement to sleep better?

-Avoiding electronic devices like mobile and laptop, one for before sleep.

- Regularity in sleep: Going to bed at same time, and waking up at the same time. Regularity improves, quantity as well as quality of sleep.
- Sleep in relatively lower temperature room. Because, body falls to sleep better in relative-cooler environment.
- Meditating for 10 minutes before the sleep.

# 5. Brain Changing Benefits of Exercise

## Question 6

## Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- The video "Brain Changing Benefits of Exercise" discusses the ways in which exercise can positively impact the brain.
- The speaker mentions that regular physical activity can increase the production of brain-derived neurotrophic factor, a protein that helps to support the growth and survival of brain cells.
- Exercise has also been shown to improve cognitive function, such as memory and decision-making skills.
- In addition to these mental benefits, exercise can also help to reduce the risk of developing conditions such as Alzheimer's disease and depression.
- The speaker encourages viewers to incorporate exercise into their daily routine in order to reap the brain-boosting benefits.

## Question 7

## What are some steps you can take to exercise more?

- Exercising daily in the morning or evening.
- Playing some sports with friends, whenever possible.
- Playing on weekends
- Taking a small walk in-between stressful work in intervals, which relaxes.

## Refrences

- [Emotional Quadrant](https://www.researchgate.net/publication/335191634/figure/fig2/AS:792212367486976@1565889555183/Modified-PA-plane-with-four-emotional-quadrants.jpg)

- [Headspace | Meditation | Brilliant things happen in calm minds](https://www.youtube.com/watch?v=lACf4O_eSt0)
- [One-Moment Meditation: "How to Meditate in a Moment"](https://www.youtube.com/watch?v=F6eFFCi12v8)
- [Sleep is your superpower | Matt Walker](https://www.youtube.com/watch?v=5MuIMqhT8DM)
- [Wendy Suzuki: The brain-changing benefits of exercise | TED](https://www.youtube.com/watch?v=BHY0FxzoKZE)
