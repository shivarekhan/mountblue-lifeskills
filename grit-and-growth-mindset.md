# Grit and Growth Mindset

## Grit

### Question 1

### Paraphrase (summarize) the video in a few lines. Use your own words.

- Speaker talks about grit and the growth mindset.
- Speaker tells about how a growth mindset and perseverance in more important than talent or a great IQ for success.
- The importance of motivation in the success of an individual.

### Question 2

### What are your key takeaways from the video to take action on?

- IQ is important for success compared to GIRT.
- Don't fall during failure but improve.
- GIRT is the hunger for the goal
- Create a growth mindset which helps to improve.
- To perseverance during failure.

## Introduction to Growth Mindset

**The belief in your capacity to learn and grow.**

### Question 3

### Paraphrase (summarize) the video in a few lines in your own words.

The video talks about the two ways of thinking and fixed mindset and the growth mindset what are they and what are their characteristics and what are the core belief of both mindsets and how both mindsets behave in different situations and how they respond to a particular situation.

### Question 4

### What are your key takeaways from the video to take action on?

- The belief in your capacity to learn and grow.
- Changing and improving the way we people learn.
- Not getting discouraged during failure.
- Not to have a fixed mindset.

## Understanding Internal Locus of Control.

### Question 5

### What is the Internal Locus of Control? What is the key point in the video?

**Locus of Control** is the degree one's belief that, one has control over their life. And **Internal Locus of Control** is the belief that How much work one put into something that one has complete control over.

**Key Point in the video:** Internal Local of Control is the key for motivation.

To be motivated all the time, one can best achieve it by simply solving the issues in one's own life, taking some time, and appreciating that one action solved the problems.

## How to build a Growth Mindset

### Question 6

### Paraphrase (summarize) the video in a few lines in your own words.

There are basically 2 kinds of mindset.

1. **Fixed Mindset** - When one thinks, things will always be the way they are, natural, and one can't grow.
2. **Growth Mindset** - When one believes that one can improve and grow through effort and actions.

Growth Mindset is popularised by **Carol Dweck** through his book 'Mindset: The New Psychology of Success'.

Gowth mindset can be developed by following methods,

- **Belief:** Belief in your ability to figure things out.
- **Questioning your Negative Assumptions:** Don't let your current knowledge & ability narrow down your future. What you are capable of doing today has nothing to do with what you can do tomorrow. Question assumptions about your ability if it's hindering your growth.
- **Develop Life-curriculum:** One should develop a long-term curriculum for long-term growth. Develop an architect for the goal. For that, buy books, attend seminars, etc., and don't wait/depend on anyone or anything for your growth.
- **Honour the struggle:** During the process, one may face many discouragement, failures, and pain. One should honour the struggle and have perseverance.

### Question 7

### What are your key takeaways from the video to take action on?

- Self-belief and self-confidence are the base for growth.
  - Have a life curriculum, and work towards that.
  - Don't get frustrated during the process, and honour the struggle. The difficulty will build a stronger character and help subsequently, so keep going.
  - To keep persistance when getting knocked out.

## Mindset - A MountBlue Warrior Reference Manual

### Question 8

### What are one or more points that you want to take action on from the manual? (Maximum 3)

- I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
- I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
- I will not write a word of code that I don’t understand.

## Resouces
- https://www.youtube.com/watch?v=H14bBuluwB8
- https://www.youtube.com/watch?v=75GFzikmRY0
- https://www.youtube.com/watch?v=9DVdclX6NzY
- https://www.youtube.com/watch?v=8ZhoeSaPF-k
- https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk

