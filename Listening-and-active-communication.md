# Listening And Active Communication

### Q1 - What are the steps/strategies to do Active Listening?
- Avoid getting distracted by your own thoughts. Focus on the speaker and topic instead.
- Try not to interrupt the other person. Let them finish and then respond.
- Use door openers. These are phrases that show your are interested and keep the other person talking.
- Show that you are listening with body language.
- If appropriate take notes during important conversations.
- Paraphrase what other have said to make sure you are on the same page.


### Q2 - According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
- Pay attention to the speaker: This means actively listening to what the speaker is saying, and not interrupting or getting sidetracked by other thoughts.
- Show that you are listening: Use nonverbal cues like nodding and making eye contact to show that you are paying attention.
- Reflect back what you have heard: Paraphrase what the speaker has said to show that you have understood their perspective.
- Empathize with the speaker: Try to understand and feel what the speaker is feeling, and express this empathy back to them.
- Ask open-ended questions: Encourage the speaker to elaborate on their thoughts and feelings by asking open-ended questions.


### Q3 - What are the obstacles in your listening process?

- **Distractions**: Anything that takes your attention away from the speaker can be a distraction and make it difficult to fully listen and understand what is being said.

- **Preconceived notions**: If you come into a conversation with fixed ideas about what the speaker is going to say, it can be difficult to listen with an open mind and fully understand their perspective.

- **Lack of focus**: If you are tired, stressed, or otherwise not fully present in the conversation, it can be difficult to listen effectively.

### Q4 - What can you do to improve your listening?
#### There are several things you can do to improve your listening skills:

- **Pay attention**: Make a conscious effort to listen and avoid distractions.
- **Avoid interrupting**: Let the other person finish speaking before you respond.
- **Take notes**: Writing down key points can help you focus and remember what was said.
- **Seek feedback**: Ask someone you trust to give you feedback on your listening skills and how you can improve.

### Q5 - When do you switch to Passive communication style in your day to day life?
 - When you feel intimidated
 - When you want to avoid conflict
 - When you feel that your opinion doesn't matter


 ### Q6 - When do you switch into Aggressive communication styles in your day to day life?
 - I keep getting bothered by someone.
 - The other person needs to accomplish something crucial, yet they keep dodging or disregarding me.
 - There is certain behavior that must be halted because it is unacceptable.

### Q7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- if someone is passive-aggressively speaking to me.
- With someone who I can’t directly argue.

### Q8 - How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

- putting your needs ahead of other people's.
- Without worrying about how others will perceive you or seeming overly aggressive, express your feelings and let them know what you really believe or want.
- Understanding our own emotions, because sometimes we don't even know what we're feeling, makes it difficult to communicate with others.
- Not to apologise without cause; when we refrain from doing so, we come out as more confident.
- expressing your issues and what's bothering you


### Refrences
- https://www.youtube.com/watch?v=rzsVh8YwZEQ
- https://en.wikipedia.org/wiki/Reflective_listening
- https://www.youtube.com/watch?v=yjOWXsDt87Y
- https://www.youtube.com/watch?v=SYuboi4GWO4
- https://www.youtube.com/watch?v=BanqlGZSWiI
- https://www.youtube.com/watch?v=vlwmfiCb-vc
