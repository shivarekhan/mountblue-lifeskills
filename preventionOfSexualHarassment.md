# Prevention of Sexual Harassment

## What kinds of behavior cause sexual harassment?
### Sexual Harassment-
Any unwelcome verbal, visual and physical conduct of a sexual nature that is severe or pervasive affects the working condition and creates a hostile environment.

### 3 Forms of Sexual Harassment
- Verbal
- Visual
- Physical

### Verbal Harassment
- Commenting about clothing
- A person's body
- Sexual or gender-based jokes
- Repedeatly asking a person out
- Foul and obscene language

### Physical Harassment
- Creating the absence poster/ Unethical drawing and picture / Cartoon and screen savers.

### Physical Harassment
- Try a sexual assault
- Impeding/blocking movement
- Inappropriate touching (such as stroking)
- Sexual gesture /leering or staring


## What would you do if you faced or witness any incident or repeated incidents of such behavior?
There are some steps mentioned below to follow in case of sexual harassment:-
- **Document the incidents** - write about the incident and the person to make a strong case to present in the case of any legal proceedings and present your complaint in front of your employer.

- **Tell the person / Warn the person** - if you are feeling strong enough you can directly talk to that person to stop this kind of activity.

- **Take care of yourself**: It's essential to prioritize your well-being after experiencing or witnessing sexual harassment. This may involve seeking counseling or therapy, finding ways to manage stress, or taking time to rest and recharge.


## Resouces
- https://www.youtube.com/watch?v=Ue3BTGW3uRQ
- https://www.youtube.com/watch?v=u7e2c6v1oDs

